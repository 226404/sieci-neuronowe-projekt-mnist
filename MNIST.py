import numpy
import NeuralNet


def train_the_neural_net(neural_net, cykle=1):
    print ('Uczenie sieci neuronowej...')
    plik_dane_do_uczenia = open('mnist_train.csv', 'r')
    dane_do_uczenia = plik_dane_do_uczenia.readlines()

    plik_dane_do_uczenia.close()

    cykle = cykle
    for i in range(cykle):
        print('Cykle treningowe {}/{}.'.format(i + 1, cykle))
        for record in dane_do_uczenia:
            wszystkie_wartosci = record.split(',')
            wejscie = (numpy.asfarray(wszystkie_wartosci[1:]) / 255.0 * 0.99) + 0.01
            cyfra_z_obrazka = numpy.zeros(output_nodes) + 0.01
            cyfra_z_obrazka[int(wszystkie_wartosci[0])] = 0.99

            neural_net.train(wejscie, cyfra_z_obrazka)

    print('Siec zostala nauczona!.')


def test_the_neural_net(neural_net):
    print('Testowanie sieci neuronowej...')
    plik_dane_do_testu = open('mnist_test.csv', 'r')
    dane_do_testu = plik_dane_do_testu.readlines()
    plik_dane_do_testu.close()

    poprawne_rozpoznania = []
    for i, record in enumerate(dane_do_testu):
        wszystkie_wartosci = record.split(',')
        poprawna_cyfra = int(wszystkie_wartosci[0])
        wejscie = (numpy.asfarray(wszystkie_wartosci[1:]) / 255.0 * 0.99) + 0.01

        outputs = neural_net.query(wejscie)

        rozpoznana_cyfra = numpy.argmax(outputs)
        if rozpoznana_cyfra == poprawna_cyfra:
            poprawne_rozpoznania.append(1)
        else:
            poprawne_rozpoznania.append(0)

    print('Przetestowano siec!')

    return poprawne_rozpoznania

def test_one_letter(neural_net):
    print('Testowanie jednego znaku...')
    plik_dane_do_testu = open('mnist_test.csv', 'r')
    dane_do_testu = plik_dane_do_testu.readline()
    plik_dane_do_testu.close()

    wszystkie_wartosci = dane_do_testu.split(',')
    poprawna_cyfra = int(wszystkie_wartosci[0])
    wejscie = (numpy.asfarray(wszystkie_wartosci[1:]) / 255.0 * 0.99) + 0.01

    wyjscie = neural_net.query(wejscie)
    rozpoznana_cyfra = numpy.argmax(wyjscie)

    print("Powinno byc: " + str(poprawna_cyfra))
    print("Rozpoznano:  " + str(rozpoznana_cyfra))

if __name__ == '__main__':

    print('Rozpoczynanie rozpoznawania odrecznego pisma przez siec neuronowa...')

    input_nodes = 784       # 28px * 28px = 784px
    hidden_nodes = 200
    output_nodes = 10
    learning_rate = 0.1

    nn = NeuralNet.NeuralNet(input_nodes, hidden_nodes, output_nodes, learning_rate)

    # Uczenie
    train_the_neural_net(nn, cykle=1)

    # Test
    wynik_testu = numpy.asarray(test_the_neural_net(nn))

    # Print results
    print('Siec neuronowa ma dokladnosc {}% w rozpoznawaniu pisma odrecznego!'
          .format(wynik_testu.sum() / float(wynik_testu.size) * 100.0))

    # Test jednego znaku
    test_one_letter(nn)
